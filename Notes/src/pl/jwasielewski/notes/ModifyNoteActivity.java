package pl.jwasielewski.notes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class ModifyNoteActivity extends Activity {
	
	private boolean isAddingNewElement;
	private EditText noteHeader;
	private EditText noteBody;
	private int position;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.note_activity);
		setupActionBar();
		noteHeader = (EditText) findViewById(R.id.edit_note_header);
		noteBody = (EditText) findViewById(R.id.edit_note_body);
		Bundle bundle = getIntent().getExtras();
		if(bundle.getString("TYPE").equals("NEW")) {
			this.getActionBar().setTitle("Dodaj notatkę");
			isAddingNewElement = true;
		} else if(bundle.getString("TYPE").equals("MODIFY")) {
			this.getActionBar().setTitle("Edytuj notatkę");
			noteHeader.setText(bundle.getString("HEADER"));
			noteBody.setText(bundle.getString("BODY"));
			position = bundle.getInt("POSITION");
			isAddingNewElement = false;
		}
	}

	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//getMenuInflater().inflate(R.menu.modify_note, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void finish() {
		Intent intent = new Intent();
		intent.putExtra("RESULT", "OK");
		intent.putExtra("HEADER", noteHeader.getText().toString());
		intent.putExtra("BODY", noteBody.getText().toString());
		if(!isAddingNewElement) {
			intent.putExtra("POSITION", position);
		}
		setResult(RESULT_OK, intent);
		super.finish();
	}
	
	public void onNoteSave(View view) {
		if(noteHeader.getText().toString().equals("")
				|| noteBody.getText().toString().equals("")) {
			return;
		}
		finish();
	}

}
