package pl.jwasielewski.notes;

import java.util.ArrayList;

import pl.jwasielewski.notes.data.Note;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NotesAdapter extends ArrayAdapter<Note> {
	private final Context context;
	private ArrayList<Note> notes;
	
	public NotesAdapter(Context c, ArrayList<Note> n) {
		super(c, R.layout.row_view, n);
		context = c;
		notes = n;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		View row = inflater.inflate(R.layout.row_view, parent, false);
		
		((TextView)row.findViewById(R.id.row_view_header)).setText(notes.get(position).header);
		((TextView)row.findViewById(R.id.row_view_body)).setText(notes.get(position).body);
		
		return row;
	}

}
