package pl.jwasielewski.notes.data;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Context;

public class NotesReader {
	
	public static ArrayList<Note> read(String file, Context c) {
		
		ArrayList<Note> notes = new ArrayList<Note>();
		
		try {
			
			File f = new File(c.getFilesDir(), file);
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(f);
			
			doc.getDocumentElement().normalize();
			
			NodeList list = doc.getElementsByTagName("note");
			
			for(int i = 0; i < list.getLength(); ++i) {
				Node node = list.item(i);
				if(node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					notes.add(new Note(
							element.getElementsByTagName("header").item(0).getTextContent(),
							element.getElementsByTagName("body").item(0).getTextContent()
							));
				}
			}
			
		} catch(Exception e) {
			
		}
		
		return notes;
	}
	
}
