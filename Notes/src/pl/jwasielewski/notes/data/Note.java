package pl.jwasielewski.notes.data;

public class Note {
	public String header;
	public String body;
	
	public Note(String h, String b) {
		header 	= h;
		body 	= b;
	}
	
	@Override
	public String toString() {
		return "H: " + header + "| B: " + body;
	}
}
