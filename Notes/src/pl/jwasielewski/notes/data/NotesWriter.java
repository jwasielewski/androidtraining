package pl.jwasielewski.notes.data;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.content.Context;

public class NotesWriter {
	
	public static void write(String file, ArrayList<Note> notes, Context c) {
		
		BufferedWriter bw = null;
		
		try {
			bw = new BufferedWriter(new OutputStreamWriter(
					c.openFileOutput(file, Context.MODE_PRIVATE)));
			
			bw.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			bw.write("<notes>\n");
			for(Note n : notes) {
				bw.write("<note>\n<header>"+n.header+"</header>\n");
				bw.write("<body>"+n.body+"</body>\n</note>\n");
			}
			bw.write("</notes>\n");
		} catch(IOException e) {
			return;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
