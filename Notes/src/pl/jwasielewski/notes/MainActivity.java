package pl.jwasielewski.notes;

import java.util.ArrayList;
import java.util.Arrays;

import pl.jwasielewski.notes.data.Note;
import pl.jwasielewski.notes.data.NotesReader;
import pl.jwasielewski.notes.data.NotesWriter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class MainActivity extends Activity {
	
	private ListView list;
	private NotesAdapter nadapter;
	private ArrayList<Note> notes = null;
	private Note[] anotes = null;
	private boolean hasNotes;
	public static final int REQUEST_CODE_ADD = 100;
	public static final int REQUEST_CODE_MODIFY = 101;
	private final MainActivity self = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		notes = new ArrayList<Note>();
		hasNotes = false;
		onShow();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onStop() {
		super.onStop();
		onClose();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode != RESULT_OK) return;
		Bundle bundle = data.getExtras();
		if(requestCode == REQUEST_CODE_ADD) {
			notes.add(new Note(bundle.getString("HEADER"), bundle.getString("BODY")));
			nadapter.notifyDataSetChanged();
			if(!hasNotes) {
				setContentView(R.layout.activity_main);
				createList();
				hasNotes = true;
			}
		} else if(requestCode == REQUEST_CODE_MODIFY) {
			anotes = notes.toArray(new Note[notes.size()]);
			anotes[bundle.getInt("POSITION", -1)].header = bundle.getString("HEADER");
			anotes[bundle.getInt("POSITION", -1)].body = bundle.getString("BODY");
			notes = new ArrayList<Note>(Arrays.asList(anotes));
			nadapter.notifyDataSetChanged();
		}
	}
	
	private void onShow() {
		notes = NotesReader.read("notes.xml", getApplicationContext());
		nadapter = new NotesAdapter(getApplicationContext(), notes);

		if(notes.size() > 0) {
			setContentView(R.layout.activity_main);
			createList();
			hasNotes = true;
		} else {
			setContentView(R.layout.empty_notes_file);
		}
	}
	
	private void onClose() {
		if(notes.size() > 0) {
			NotesWriter.write("notes.xml", notes, getApplicationContext());
		}
	}
	
	public void onButtonClick(View view) {
		Intent intent = new Intent(this, ModifyNoteActivity.class);
		intent.putExtra("TYPE", "NEW");
		startActivityForResult(intent, REQUEST_CODE_ADD);
	}
	
	private void createList() {
		list = (ListView) findViewById(R.id.main_list);
		list.setAdapter(nadapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent(getApplicationContext(),
						ModifyNoteActivity.class);
				anotes = notes.toArray(new Note[notes.size()]);
				intent.putExtra("TYPE", "MODIFY");
				intent.putExtra("POSITION", position);
				intent.putExtra("HEADER", anotes[position].header);
				intent.putExtra("BODY", anotes[position].body);
				startActivityForResult(intent, REQUEST_CODE_MODIFY);
			}
		
		});
		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, final View view,
					final int position, long id) {
				
				view.animate().setDuration(300).scaleYBy(-1f).scaleXBy(-1f)
				.withEndAction(new Runnable() {

					@Override
					public void run() {
						view.setScaleY(1);
						view.setScaleX(1);
						notes.remove(position);
						nadapter.notifyDataSetChanged();
						if(notes.size() == 0) {
							hasNotes = false;
							self.setContentView(R.layout.empty_notes_file);
						}
					}
					
				});
				
				return true;
			}
		});
	}

}
