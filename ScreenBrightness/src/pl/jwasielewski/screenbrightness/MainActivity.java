package pl.jwasielewski.screenbrightness;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class MainActivity extends Activity {

	private WindowManager.LayoutParams lp;
	private SeekBar sb;
	private TextView tv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		lp = getWindow().getAttributes();
		tv = (TextView) findViewById(R.id.textView);
		
		sb = (SeekBar) findViewById(R.id.seekBar);
		sb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				if(fromUser) {
					lp = getWindow().getAttributes();
					lp.screenBrightness = progress / 100f;
					tv.setText("+"+progress);
					getWindow().setAttributes(lp);
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}
	
	public void setOn(View v) {
		lp.screenBrightness = 1f;
		getWindow().setAttributes(lp);
		tv.setText("100");
		sb.setProgress(100);
	}
	
	public void setOff(View v) {
		lp.screenBrightness = 0f;
		getWindow().setAttributes(lp);
		tv.setText("0");
		sb.setProgress(0);
	}

}
