package pl.jwasielewski.endlesslistview;

import java.util.ArrayList;
import java.util.List;

import pl.jwasielewski.endlesslistview.EndlessListView.EndlessListener;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;

public class MainActivity extends Activity implements EndlessListener {
	
	private final static int ITEM_PER_REQUEST = 50;
	private EndlessListView lv;
	private int mult = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		lv = (EndlessListView) findViewById(R.id.el);
		
		EndlessAdapter adp = new EndlessAdapter(this, createItems(mult), R.layout.row_layout);
		lv.setLoadingView(R.layout.loading_layout);
		lv.setAdapter(adp);
		lv.setListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private class FakeNetLoader extends AsyncTask<String, Void, List<String>> {

		@Override
		protected List<String> doInBackground(String... params) {
			try {
				Thread.sleep(4000);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
			return createItems(mult);
		}
		
		@Override
		protected void onPostExecute(List<String> result) {
			super.onPostExecute(result);
			lv.addNewData(result);
		}
		
	}
	
	private List<String> createItems(int mult2) {
		List<String> list = new ArrayList<String>();
		for(int i=0; i<ITEM_PER_REQUEST; ++i) {
			list.add("Item " + (i * mult2));
		}
		return list;
	}

	@Override
	public void loadData() {
		mult += 10;
		FakeNetLoader fl = new FakeNetLoader();
		fl.execute(new String[]{});
	}

}
