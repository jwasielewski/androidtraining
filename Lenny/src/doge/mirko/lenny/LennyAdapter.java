package doge.mirko.lenny;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class LennyAdapter extends ArrayAdapter<String> implements OnItemClickListener {

	private String[] items;
	private Context context;
	
	public LennyAdapter(Context context, String[] values) {
		super(context, R.layout.row, values);
		this.context = context;
		items = values;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = 
				(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.row, parent, false);
		
		TextView tv = (TextView) view.findViewById(R.id.row_text);
		tv.setText(items[position]);
		
		return view;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		ClipboardManager clipboard = 
				(ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE); 
		ClipData clip = ClipData.newPlainText("label", items[position]);
		clipboard.setPrimaryClip(clip);
		Toast.makeText(context, context.getString(R.string.copied), Toast.LENGTH_SHORT)
			.show();
	}

}
