package doge.mirko.lenny;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ArrayList<String> als = readFile();
		
		ListView lv = (ListView) findViewById(R.id.main_list);
		final LennyAdapter la = new LennyAdapter(getApplicationContext(),
				als.toArray(new String[als.size()]));
		
		lv.setAdapter(la);
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				la.onItemClick(arg0, arg1, arg2, arg3);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private ArrayList<String> readFile() {
		ArrayList<String> t = new ArrayList<String>();
		try {
		    BufferedReader inputReader = new BufferedReader(new InputStreamReader(
		            openFileInput("strings.txt")));
		    String inputString;
		    while ((inputString = inputReader.readLine()) != null) {
		        t.add(inputString);
		    }
		} catch (IOException e) {
		    e.printStackTrace();
		}
		return (t.size() > 0) ? t : createDefaultFile();
	}
	
	private ArrayList<String> createDefaultFile() {
		ArrayList<String> t = new ArrayList<String>();
		t.add("( ͡° ͜ʖ ͡°) ");
		t.add("( ͡° ʖ̯ ͡°)");
		t.add("( ͡º ͜ʖ͡º)");
		t.add("( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°) ");
		t.add("(⌐ ͡■ ͜ʖ ͡■)");
		t.add("(・へ・)");
		t.add("(╥﹏╥)");
		t.add("(╯°□°）╯︵ ┻━┻");
		t.add("(ʘ‿ʘ)");
		t.add("(｡◕‿‿◕｡)");
		t.add("ᕙ(⇀‸↼‶)ᕗ");
		t.add("ᕦ(ò_óˇ)ᕤ");
		t.add("(✌ ﾟ ∀ ﾟ)☞");
		t.add("t(ツ)_/¯");
		t.add("﴾͡๏̯͡๏﴿");
		t.add("ب_ب");
		t.add("ʕ•ᴥ•ʔ");
		t.add("(⌒(oo)⌒)");
		t.add("ᄽὁȍ ̪ őὀᄿ");
		t.add("( ͡€ ͜ʖ ͡€)");
		t.add("( ͡° ͜ʖ ͡°)ﾉ⌐■-■ ");
		t.add("(⌐ ͡■ ͜ʖ ͡■)");
		t.add("(。ヘ°)");
		t.add("(︶︹︺)");
		t.add("(∪_∪)｡｡｡zzz");
		t.add("(ᵔᴥᵔ)");
		t.add("(¬‿¬)");
		t.add("(ﾉ´ヮ´)ﾉ*:･ﾟ✧");
		t.add("ヾ(⌐■_■)ノ♪");
		t.add("(ง •̀_•́)ง");
		t.add("usuń konto");
		t.add("Dd");
		t.add("ŁAN");
		
		try {
		    FileOutputStream fos = openFileOutput("strings.txt",Context.MODE_APPEND);
		    for(String s : t) {
		    	fos.write((s+'\n').getBytes());
		    }
		    fos.close();
		} catch (Exception e) {
		    e.printStackTrace();
		}
		
		return t;
	}

}
