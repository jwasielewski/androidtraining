package pl.jwasielewski.myalarmclock;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MySimpleArrayAdapter extends ArrayAdapter<String> {

	private Context context;
	private final String[] values;
	
	public MySimpleArrayAdapter(Context context, String[] values) {
		super(context, R.layout.list_row_1, values);
		this.context = context;
		this.values = values;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView1 = inflater.inflate(R.layout.list_row_1, parent, false);
		View rowView2 = inflater.inflate(R.layout.list_row_2, parent, false);
		TextView textView;
		ImageButton button;
		
		if(position % 2 == 0) {
			textView = (TextView) rowView1.findViewById(R.id.alarmTime);
			button = (ImageButton) rowView1.findViewById(R.id.changeStateButton);
			rowView1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(final View v) {
					v.animate().setDuration(2000).rotationX(360f)
					.withEndAction(new Runnable() {
						@Override
						public void run() {
							v.setRotationX(0f);
						}
					});
				}
			
			});
		} else {
			textView = (TextView) rowView2.findViewById(R.id.alarmTime);
			button = (ImageButton) rowView2.findViewById(R.id.changeStateButton);
			rowView2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(final View v) {
					v.animate().setDuration(2000).rotationY(360f)
					.withEndAction(new Runnable() {
						@Override
						public void run() {
							v.setRotationY(0f);
						}
					});
				}
			
			});
		}
		
		textView.setText(values[position]);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View view) {
				view.animate().setDuration(2000).rotation(360f)
				.withEndAction(new Runnable() {
					@Override
					public void run() {
						view.setRotation(0f);
					}
				});
				Toast.makeText(context, values[position]+" at "+position, 
						Toast.LENGTH_SHORT)
						.show();
			}
			
		});
		
		return ((position % 2 == 0) ? rowView1 : rowView2);
	}
	
}
