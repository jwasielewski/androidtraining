package pl.jwasielewski.listviewtraining;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

@SuppressLint("UseSparseArrays")
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class MainActivity extends Activity {

	final MainActivity self = this;
	public static String OS_NAME = "OS_NAME";
	private final HashMap<Integer, Boolean> anim = new HashMap<Integer, Boolean>();
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        final ListView listview = (ListView) findViewById(R.id.listview);
        final ArrayList<String> list = new ArrayList<String>();
        String[] values = new String[] {
        		"Android", "iPhone", "WindowsMobile",
                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                "Linux", "OS/2", "Ubuntu", "Windows7", "Max OS X", "Linux",
                "OS/2", "Ubuntu", "Windows7", "Max OS X", "Linux", "OS/2",
                "Android", "iPhone", "WindowsMobile"
        };
        
	    for(int i=0; i < values.length; ++i) {
	     	list.add(values[i]);
	       	anim.put(i, false);
	    }
	        
        
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
        		this,
        		R.layout.row_layout,
        		R.id.label,
        		list);
        listview.setAdapter(adapter);
        
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view, 
					final int position, long id) {
				float i = (position % 2 == 0) ? 1f : -1f;
				final String item = (String) parent.getItemAtPosition(position);
				anim.put(position, !anim.get(position));
				Log.wtf("Anim", position+" "+anim.get(position)+"");
				view.animate().setDuration(3000).rotation(i*360f)
					.withEndAction(new Runnable() {
						@Override
						public void run() {
							Intent intent = new Intent(self, DetailActivity.class);
							intent.putExtra(OS_NAME, list.get(position));
							
							list.remove(item);
							adapter.notifyDataSetChanged();
							view.setRotation(0f);
							
							self.startActivity(intent);
						}
					});
				Toast.makeText(getApplicationContext(), "Click ListItem Number "
						+ position, Toast.LENGTH_SHORT)
						.show();
			}
		});
        
    }
    
    @SuppressWarnings("unused")
	private class StableArrayAdapter extends ArrayAdapter<String> {
    	
    	HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
    	
    	public StableArrayAdapter(Context context,
    			int textViewResourceId,
    			List<String> objects) {
    		super(context, textViewResourceId, objects);
    		for(int i = 0; i < objects.size(); ++i) {
    			mIdMap.put(objects.get(i), i);
    		}
    	}
    	
    	@Override
    	public long getItemId(int position) {
    		String item = getItem(position);
    		return mIdMap.get(item);
    	}
    	
    	@Override
    	public boolean hasStableIds() {
    		return true;
    	}
    	
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
