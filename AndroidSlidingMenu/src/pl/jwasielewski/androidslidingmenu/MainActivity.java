package pl.jwasielewski.androidslidingmenu;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

	private LinearLayout ll;
	private float startY, endY;
	private Animation animUp, animDown;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ll = (LinearLayout) findViewById(R.id.slider);
		ll.setVisibility(View.GONE);
		
		startY = endY = 0;
		
		animUp = AnimationUtils.loadAnimation(this, R.animator.anim_up);
		animDown = AnimationUtils.loadAnimation(this, R.animator.anim_down);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		switch(event.getAction()) {
		
		case MotionEvent.ACTION_DOWN:
			startY = event.getY();
			break;
		
		case MotionEvent.ACTION_UP:
			endY = event.getY();
			if(endY < startY) {
				ll.setVisibility(View.VISIBLE);
				ll.startAnimation(animUp);
			} else {
				ll.startAnimation(animDown);
				ll.setVisibility(View.GONE);
			}
			break;
			
		}
		
		return true;
	}

}
