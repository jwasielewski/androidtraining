package pl.jwasielewski.app;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.IBinder;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ChatBubbleService extends Service {

    private WindowManager mWindowManager;
    private View mChatHead;
    private ImageView mChatHeadImageView;
    private TextView mChatHeadTextView;
    private LinearLayout mLayout;
    private static int screenHeight;
    private static int screenWidth;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //Typeface tf = Typeface.createFromAsset(getAssets(), "Roboto-Light.tff");

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = mWindowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;

        LayoutInflater inflater = LayoutInflater.from(this);
        mChatHead = inflater.inflate(R.layout.chathead, null);
        //assert mChatHead != null;
        mChatHeadImageView = (ImageView) mChatHead.findViewById(R.id.chathead_imageview);
        mChatHeadTextView = (TextView) mChatHead.findViewById(R.id.chathead_textview);
        mLayout = (LinearLayout) mChatHead.findViewById(R.id.chathead_linearlayout);

        //mChatHeadTextView.setTypeface(tf);

        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT
        );

        parameters.x = 0;
        parameters.y = 50;
        parameters.gravity = Gravity.TOP | Gravity.LEFT;

        mChatHeadImageView.setOnTouchListener(new View.OnTouchListener() {

            int initialX, initialY;
            float initialTouchX, initialTouchY;

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                switch(motionEvent.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        initialX = parameters.x;
                        initialY = parameters.y;
                        initialTouchX = motionEvent.getRawX();
                        initialTouchY = motionEvent.getRawY();
                        Toast.makeText(getApplication(), "Drag it here to remove", Toast.LENGTH_SHORT)
                            .show();
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mChatHeadTextView.setVisibility(View.GONE);
                        parameters.x = initialX + (int)(motionEvent.getRawX() - initialTouchX);
                        parameters.y = initialY + (int)(motionEvent.getRawY() - initialTouchY);
                        mWindowManager.updateViewLayout(mChatHead, parameters);
                        return true;

                    case MotionEvent.ACTION_UP:

                        if(parameters.y > screenHeight * 0.6) {
                            mChatHead.setVisibility(View.GONE);
                            Toast.makeText(getApplication(), "Removed!", Toast.LENGTH_SHORT)
                                    .show();
                            stopSelf();
                        }

                        if(parameters.x < screenWidth / 2) {
                            mLayout.removeAllViews();
                            mLayout.addView(mChatHeadImageView);
                            mLayout.addView(mChatHeadTextView);
                            mChatHeadTextView.setVisibility(View.VISIBLE);
                        } else {
                            mLayout.removeAllViews();
                            mLayout.addView(mChatHeadTextView);
                            mLayout.addView(mChatHeadImageView);
                            mChatHeadTextView.setVisibility(View.VISIBLE);
                        }

                        return true;

                    default:
                        return false;
                }

            }
        });

        mWindowManager.addView(mChatHead, parameters);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mChatHead != null) {
            mWindowManager.removeView(mChatHead);
        }
    }
}
