package pl.jwasielewski.intentstraining;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);
		String value = getIntent().getExtras().getString("VALUE");
		TextView textview = (TextView) findViewById(R.id.displayintentextra);
		textview.setText(value);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.result, menu);
		return true;
	}
	
	@Override
	public void finish() {
		Intent intent = new Intent();
		String value = ((EditText)findViewById(R.id.returnValue))
				.getText().toString();
		intent.putExtra("returnkey", value);
		setResult(RESULT_OK, intent);
		super.finish();
	}

}
