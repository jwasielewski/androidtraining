package pl.jwasielewski.traceractivity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		notify("onCreate");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		notify("onPause");
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		notify("onResume");
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		notify("onStop");
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		notify("onDestroy");
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle b) {
		super.onRestoreInstanceState(b);
		notify("onRestoreInstanceState");
	}
	
	@Override
	protected void onSaveInstanceState(Bundle b) {
		super.onSaveInstanceState(b);
		notify("onSaveInstanceState");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private void notify(String methodName) {
		String name = this.getClass().getName();
		String[] strings = name.split("\\.");
		Notification noti = new Notification.Builder(this)
			.setContentTitle(methodName + " " + strings[strings.length - 1])
			.setAutoCancel(true)
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentText(name)
			.build();
		NotificationManager notificationManager = 
				(NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify((int) System.currentTimeMillis(), noti);
	}

}
