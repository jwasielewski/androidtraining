package pl.jwasielewski.colorpicker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private final MainActivity self = this;
	public final static String TAG = "MainActivity";
	
	private SeekBar sbr;
	private SeekBar sbg;
	private SeekBar sbb;
	private TextView color_view;
	private String current_color;
	private int red;
	private int green;
	private int blue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		red = green = blue = 0;
		color_view = (TextView) findViewById(R.id.color_view);
		updateColor();
		
		color_view.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ClipboardManager clipboard = 
						(ClipboardManager) self.getSystemService(Context.CLIPBOARD_SERVICE); 
				ClipData clip = ClipData.newPlainText("label", current_color);
				clipboard.setPrimaryClip(clip);
				Toast.makeText(getApplicationContext(), current_color, Toast.LENGTH_SHORT)
				.show();
			}
			
		});
		
		sbr = (SeekBar) findViewById(R.id.seekbar_red);
		sbr.getProgressDrawable().setColorFilter(0xFFFF000C, PorterDuff.Mode.SRC_IN);
		sbr.getThumb().setColorFilter(0xFFFF000C, PorterDuff.Mode.SRC_IN);
		sbr.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				self.red = progress;
				self.updateColor();
			}
		});
		
		sbg = (SeekBar) findViewById(R.id.seekbar_green);
		sbg.getProgressDrawable().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		sbg.getThumb().setColorFilter(0xFF00FF00, PorterDuff.Mode.MULTIPLY);
		sbg.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				self.green = progress;
				self.updateColor();
			}
		});
		
		sbb = (SeekBar) findViewById(R.id.seekbar_blue);
		sbb.getProgressDrawable().setColorFilter(0xFF0000FF, PorterDuff.Mode.MULTIPLY);
		sbb.getThumb().setColorFilter(0xFF0000FF, PorterDuff.Mode.MULTIPLY);
		sbb.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				self.blue = progress;
				self.updateColor();
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@SuppressLint("DefaultLocale")
	private void updateColor() {
		String r, g, b;
		
		r = Integer.toHexString(red);
		g = Integer.toHexString(green);
		b = Integer.toHexString(blue);
		
		if(r.length() == 1) r = "0" + r;
		if(g.length() == 1) g = "0" + g;
		if(b.length() == 1) b = "0" + b;
		
		int bgColor = (int) Long.parseLong("ff"+r+g+b,16);
		int fgColor = (int) (0xffffffff - Long.parseLong(r+g+b, 16));
		
		current_color = ("#"+r+g+b).toUpperCase();
		color_view.setBackgroundColor(bgColor);
		color_view.setTextColor(fgColor);
		color_view.setText(current_color);
	}
	
	@SuppressLint("DefaultLocale")
	private int hextodec(String s) {
		if(s.toLowerCase().equals("f")) { return 15; }
		else if(s.toLowerCase().equals("e")) { return 14; }
		else if(s.toLowerCase().equals("d")) { return 13; }
		else if(s.toLowerCase().equals("c")) { return 12; }
		else if(s.toLowerCase().equals("b")) { return 11; }
		else if(s.toLowerCase().equals("a")) { return 10; }
		else { return Integer.parseInt(s); }
	}

}
